<?php

namespace App\Events;

use App\Mail\mensaje;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class Replicar
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $ciclo;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($ciclo)
    {
        for($i=1; $i<= $ciclo;$i++){
            Mail::to('jcuribe@legops.com')->send(new mensaje);
        }
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
