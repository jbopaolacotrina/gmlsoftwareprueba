<?php

namespace App\Http\Controllers;

use App\Events\Replicar;
use App\Mail\mensaje;
use Illuminate\Http\Request;

class PruebaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($n)
    {
        $primos = [];

        for($i=0; $i <= $n ; $i++){
            if($i< 2){
                $primos[$i] = false;
            }

            if(!isset($primos[$i])){

                $primos[$i] = true;


                for($j= $i+$i; $j <=$n; $j += $i){
                    $primos[$j] = false;
                }
            }

        }

        $cantidad = 0;


        for($p=0;$p< count($primos); $p++){
            if($primos[$p] == true){
                $cantidad ++;
            }
        }

       return "Hay ".$cantidad."  numeros primos  entre 1 a ".$n;



    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function retomar($n)
    {
        for($i=1; $i<= $n;$i++){
           return $this->index(100);
        }
        event(new Replicar($n));



    }

}
